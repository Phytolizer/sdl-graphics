//
// Created by kyle on 8/31/19.
//

#include "TtfException.h"
#include <SDL2/SDL_ttf.h>
using std::string;
std::string TtfException::make_what(const char *ttfError) {
    return string("SDL_ttf function failed: ") + ttfError;
}
TtfException::TtfException()
    : std::runtime_error(make_what(TTF_GetError())), ttfError_(TTF_GetError()) {
}
std::string TtfException::getTtfError() const {
    return ttfError_;
}
