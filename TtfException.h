//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_TTFEXCEPTION_H
#define SUDOKU_TTFEXCEPTION_H

#include <stdexcept>
class TtfException : public std::runtime_error {
private:
    std::string ttfError_;
    static std::string make_what(const char* ttfError);
public:
    TtfException();
    std::string getTtfError() const;
};

#endif // SUDOKU_TTFEXCEPTION_H
