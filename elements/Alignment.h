//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_ALIGNMENT_H
#define SUDOKU_ALIGNMENT_H

enum AlignmentHorizontal { LEFT, CENTER_H, RIGHT };

enum AlignmentVertical { TOP, CENTER_V, BOTTOM };

struct Alignment {
    AlignmentHorizontal alignH;
    AlignmentVertical alignV;
    Alignment()
        : alignH(AlignmentHorizontal::LEFT), alignV(AlignmentVertical::TOP) {}
    Alignment(AlignmentHorizontal alignH, AlignmentVertical alignV)
        : alignH(alignH), alignV(alignV) {}
};

#endif // SUDOKU_ALIGNMENT_H
