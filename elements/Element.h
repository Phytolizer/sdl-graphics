//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_ELEMENT_H
#define SUDOKU_ELEMENT_H

#include "Alignment.h"
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_render.h>
#include <functional>
class Element {
protected:
    int x_;
    int y_;
    int w_;
    int h_;
    Alignment alignment_;
    bool visible_ = false;
    std::function<void(SDL_Event &)> customEventHandler_;

public:
    Element() : x_(0), y_(0), w_(0), h_(0) {}
    Element(int x, int y, int w, int h);
    virtual ~Element() = default;
    int getX() const;
    virtual void setX(int x);
    int getY() const;
    virtual void setY(int y);
    int getW() const;
    virtual void setW(int w);
    int getH() const;
    virtual void setH(int h);
    const Alignment &getAlignment() const;
    virtual void setAlignment(const Alignment &alignment);
    bool isVisible() const;
    virtual void setVisible(bool visible);
    virtual void
    setCustomEventHandler(std::function<void(SDL_Event &)> customEventHandler);
    virtual void handleEvent(SDL_Event &event);
    virtual void draw(SDL_Renderer *renderer) = 0;
};

#endif // SUDOKU_ELEMENT_H
