//
// Created by kyle on 8/31/19.
//

#include "ButtonElement.h"
#include "../SdlException.h"
#include "../TtfException.h"
#include "../graphics/SizedFont.h"
#include "../graphics/tools.h"
void ButtonElement::draw(SDL_Renderer *renderer) {
    if (!visible_) {
        return;
    }
    // draw background
    int err = setRenderDrawColor(renderer, fillColor_);
    if (err != 0) {
	throw SdlException();
    }
    SDL_Rect buttonRect{x_, y_, w_, h_};
    err = SDL_RenderFillRect(renderer, &buttonRect);
    if (err != 0) {
	throw SdlException();
    }
    // draw text
    err = setRenderDrawColor(renderer, textColor_);
    if (err != 0) {
	throw SdlException();
    }
    Rect textRect{};
    // drawing text is expensive, only do it when necessary
    if (fontSizeChanged_) {
	SDL_Surface *textSurface = TTF_RenderText_Blended(
	    font_->getFont(fontSize_), text_.c_str(), textColor_.sdl());
	if (textSurface == nullptr) {
	    throw TtfException();
	}
	Size textBounds{textSurface->w, textSurface->h};
	textRect = {x_ + (w_ - textBounds.w) / 2, y_ + (h_ - textBounds.h) / 2,
	            textBounds.w, textBounds.h};
	if (textTexture_ != nullptr) {
	    SDL_DestroyTexture(textTexture_);
	}
	textTexture_ = SDL_CreateTextureFromSurface(renderer, textSurface);
	if (textTexture_ == nullptr) {
	    throw SdlException();
	}
    }
    SDL_Rect textSdlRect = textRect.sdl();
    err = SDL_RenderCopy(renderer, textTexture_, nullptr, &textSdlRect);
    if (err != 0) {
	throw SdlException();
    }
    // draw border
    err = setRenderDrawColor(renderer, borderColor_);
    if (err != 0) {
	throw SdlException();
    }
    err = SDL_RenderDrawRect(renderer, &buttonRect);
    if (err != 0) {
        throw SdlException();
    }
    // draw overlay
    err = setRenderDrawColor(renderer, overlays_[overlay_]);
    if (err != 0) {
	throw SdlException();
    }
    err = SDL_RenderFillRect(renderer, &buttonRect);
    if (err != 0) {
	throw SdlException();
    }
}
const Color *ButtonElement::getOverlays() const {
    return overlays_;
}
bool ButtonElement::isClicking() const {
    return clicking_;
}
void ButtonElement::setClicking(bool clicking) {
    clicking_ = clicking;
    if (clicking) {
	overlay_ = button::CLICKING;
    } else {
	overlay_ = hovering_ ? button::HOVERING : button::CLEAR;
    }
}
bool ButtonElement::isHovering() const {
    return hovering_;
}
void ButtonElement::setHovering(bool hovering) {
    hovering_ = hovering;
    if (!clicking_) {
	overlay_ = hovering ? button::HOVERING : button::CLEAR;
    }
}
const Color &ButtonElement::getFillColor() const {
    return fillColor_;
}
void ButtonElement::setFillColor(const Color &fillColor) {
    fillColor_ = fillColor;
}
const Color &ButtonElement::getTextColor() const {
    return textColor_;
}
void ButtonElement::setTextColor(const Color &textColor) {
    textColor_ = textColor;
}
const std::string &ButtonElement::getText() const {
    return text_;
}
void ButtonElement::setText(const std::string &text) {
    text_ = text;
    if (font_ == nullptr) {
	throw std::runtime_error("Font must be provided for text rendering");
    }
    updateFontSize();
}
const std::function<void()> &ButtonElement::getClickAction() const {
    return clickAction_;
}
void ButtonElement::setClickAction(const std::function<void()> &clickAction) {
    clickAction_ = clickAction;
}
int ButtonElement::getMaxFontSize() const {
    return maxFontSize_;
}
void ButtonElement::setMaxFontSize(int maxFontSize) {
    maxFontSize_ = maxFontSize;
}
ButtonElement::ButtonElement(SizedFont *font)
    : Element(0, 0, 0, 0), fillColor_(color::WHITE), overlay_(0), text_(""),
      clicking_(false), hovering_(false), textColor_(color::BLACK),
      maxFontSize_(-1), initialClickPosition_(nullptr), fontSize_(0),
      font_(font), fontSizeChanged_(false), textTexture_(nullptr),
      borderColor_(color::BLACK) {}
void ButtonElement::sizeUpdated() {
    updateFontSize();
}
void ButtonElement::updateFontSize() {
    int newFontSize;
    if (h_ == 0 || w_ == 0) {
	newFontSize = 0;
    } else {
	double textFillProp = 0.667;
	int initialSize = static_cast<int>(h_ * textFillProp);
	Size textSize{};
	int err = TTF_SizeText(font_->getFont(initialSize), text_.c_str(),
	                       &textSize.w, &textSize.h);
	if (err != 0) {
	    throw TtfException();
	}
	int maxWidth = static_cast<int>(w_ * textFillProp);
	if (textSize.w > maxWidth) {
	    // need to shrink to fit the width
	    double heightWidthRatio =
	        static_cast<double>(textSize.h) / textSize.w;
	    newFontSize = static_cast<int>(heightWidthRatio * maxWidth);
	} else if (maxFontSize_ != -1 && initialSize > maxFontSize_) {
	    newFontSize = maxFontSize_;
	} else {
	    newFontSize = initialSize;
	}
    }
    if (newFontSize != fontSize_) {
        fontSize_ = newFontSize;
        fontSizeChanged_ = true;
    }
}
void ButtonElement::handleEvent(SDL_Event &event) {
    if (!visible_) {
        return;
    }
    Point mousePos{};
    if (event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP ||
        event.type == SDL_MOUSEMOTION) {
	SDL_GetMouseState(&mousePos.x, &mousePos.y);
    }
    bool inBounds = tools::inBounds({x_, y_, w_, h_}, mousePos);
    switch (event.type) {
    case SDL_MOUSEMOTION:
	if (clicking_) {
	    // clicking ignores any bounds check
	    break;
	}
	if (inBounds) {
	    // transition from clear to hovering
	    if (!hovering_) {
		setHovering(true);
	    }
	} else {
	    // transition from hovering to clear
	    if (hovering_) {
		setHovering(false);
	    }
	}
	break;
    case SDL_MOUSEBUTTONDOWN:
	initialClickPosition_ = new Point(mousePos);
	if (inBounds) {
	    setClicking(true);
	}
	break;
    case SDL_MOUSEBUTTONUP:
	// click must have started on this button and ended on this button
	// allows user to cancel a click by moving the mouse away
	if (initialClickPosition_ &&
	    tools::inBounds({x_, y_, w_, h_}, *initialClickPosition_) &&
	    inBounds) {
	    clickAction_();
	}
	delete initialClickPosition_;
	initialClickPosition_ = nullptr;
	setClicking(false);
	break;
    }
    // call custom event handler
    Element::handleEvent(event);
}
void ButtonElement::setW(int w) {
    Element::setW(w);
    sizeUpdated();
}
void ButtonElement::setH(int h) {
    Element::setH(h);
    sizeUpdated();
}
const Color &ButtonElement::getBorderColor() const {
    return borderColor_;
}
void ButtonElement::setBorderColor(const Color &borderColor) {
    borderColor_ = borderColor;
}
