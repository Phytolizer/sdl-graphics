//
// Created by kyle on 8/31/19.
//

#include "Element.h"

int Element::getX() const {
    return x_;
}
void Element::setX(int x) {
    switch (alignment_.alignH) {
    case LEFT:
	break;
    case CENTER_H:
	x -= static_cast<int>(w_) / 2;
	break;
    case RIGHT:
	x -= w_;
	break;
    }
    x_ = x;
}
int Element::getY() const {
    return y_;
}
void Element::setY(int y) {
    switch (alignment_.alignV) {
    case TOP:
	break;
    case CENTER_V:
	y -= static_cast<int>(h_) / 2;
	break;
    case BOTTOM:
	y -= h_;
	break;
    }
    y_ = y;
}
int Element::getW() const {
    return w_;
}
void Element::setW(int w) {
    w_ = w;
}
int Element::getH() const {
    return h_;
}
void Element::setH(int h) {
    h_ = h;
}
const Alignment &Element::getAlignment() const {
    return alignment_;
}
void Element::setAlignment(const Alignment &alignment) {
    alignment_ = alignment;
}
bool Element::isVisible() const {
    return visible_;
}
void Element::setVisible(bool visible) {
    visible_ = visible;
}
void Element::setCustomEventHandler(
    std::function<void(SDL_Event &)> customEventHandler) {
    customEventHandler_ = std::move(customEventHandler);
}
void Element::handleEvent(SDL_Event &event) {
    customEventHandler_(event);
}
Element::Element(int x, int y, int w, int h)
    : x_(x), y_(y), w_(w), h_(h), customEventHandler_([](SDL_Event &) {}) {}
