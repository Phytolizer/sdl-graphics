//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_BUTTONELEMENT_H
#define SUDOKU_BUTTONELEMENT_H

#include "../graphics/SizedFont.h"
#include "../graphics/primitives.h"
#include "Element.h"
#include <SDL2/SDL_ttf.h>
namespace button {
const Color OVERLAYS[3]{{0, 0, 0, 0}, {0, 0, 0, 64}, {0, 0, 0, 128}};
const int CLEAR = 0;
const int HOVERING = 1;
const int CLICKING = 2;
}
class ButtonElement : public Element {
private:
    Color overlays_[3]{button::OVERLAYS[0], button::OVERLAYS[1],
                       button::OVERLAYS[2]};
    bool clicking_, hovering_;
    Color fillColor_;
    Color borderColor_;

public:
    const Color &getBorderColor() const;
    void setBorderColor(const Color &borderColor);

private:
    int overlay_;
    Color textColor_;
    std::string text_;
    SizedFont *font_;
    std::function<void()> clickAction_;
    Point *initialClickPosition_;
    int fontSize_;
    int maxFontSize_;
    bool fontSizeChanged_;
    SDL_Texture *textTexture_;

    void sizeUpdated();
    void updateFontSize();

public:
    explicit ButtonElement(SizedFont *textFont);
    const Color *getOverlays() const;
    bool isClicking() const;
    void setClicking(bool clicking);
    bool isHovering() const;
    void setHovering(bool hovering);
    const Color &getFillColor() const;
    void setFillColor(const Color &fillColor);
    const Color &getTextColor() const;
    void setTextColor(const Color &textColor);
    const std::string &getText() const;
    void setText(const std::string &text);
    const std::function<void()> &getClickAction() const;
    void setClickAction(const std::function<void()> &clickAction);
    int getMaxFontSize() const;
    void setMaxFontSize(int maxFontSize);
    void setW(int w) override;
    void setH(int h) override;
    void handleEvent(SDL_Event &event) override;
    void draw(SDL_Renderer *renderer) override;
};

#endif // SUDOKU_BUTTONELEMENT_H
