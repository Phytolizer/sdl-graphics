#include "SdlException.h"
#include "TtfException.h"
#include "screens/MainScreen.h"
#include "screens/Screen.h"
#include <SDL2/SDL.h>
#include <iostream>

SizedFont font;
Screen **SCREENS;
const int MAIN_SCREEN = 0;

int main() {
    int err;
    err = SDL_Init(SDL_INIT_VIDEO);
    if (err != 0) {
	throw SdlException();
    }
    err = TTF_Init();
    if (err != 0) {
	throw TtfException();
    }
    font = SizedFont("./arial.ttf");
    SCREENS = new Screen *{new MainScreen(&font)};
    SDL_Window *window = SDL_CreateWindow("Sudoku", SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED, 800, 600,
                                          SDL_WINDOW_RESIZABLE);
    if (window == nullptr) {
	throw SdlException();
    }
    SDL_Renderer *renderer = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {
	throw SdlException();
    }
    err = SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    if (err < 0) {
	throw SdlException();
    }
    bool quit = false;
    int screen = MAIN_SCREEN;
    try {
	while (!quit) {
	    SDL_Event e;
	    while (SDL_PollEvent(&e) != 0) {
		switch (e.type) {
		case SDL_QUIT:
		    quit = true;
		    break;
		}
		SCREENS[screen]->handleEvent(e);
	    }
	    if (SDL_RenderClear(renderer) < 0) {
		throw SdlException();
	    }
	    SCREENS[screen]->draw(renderer);
	    SDL_RenderPresent(renderer);
	}
    } catch (const SdlException &e) {
	std::cout << e.what() << '\n';
	exit(1);
    } catch (const TtfException &e) {
	std::cout << e.what() << '\n';
	exit(1);
    }
    return 0;
}