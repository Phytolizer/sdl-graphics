//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_MAINSCREEN_H
#define SUDOKU_MAINSCREEN_H

#include "../elements/ButtonElement.h"
#include "../graphics/SizedFont.h"
#include "Screen.h"
class MainScreen : public Screen {
private:
    ButtonElement myButton_ = ButtonElement(nullptr);
public:
    explicit MainScreen(SizedFont* font);
    void draw(SDL_Renderer* renderer) override;
};

#endif // SUDOKU_MAINSCREEN_H
