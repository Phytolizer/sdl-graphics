//
// Created by kyle on 8/31/19.
//
#include "Screen.h"
void Screen::handleEvent(SDL_Event &event) {
    eventHandler(event);
    for (Element *element : elements_) {
        if (element) {
            element->handleEvent(event);
        }
    }
}
Screen::~Screen() {
    for (Element *element : elements_) {
        delete element;
    }
}
