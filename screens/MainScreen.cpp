//
// Created by kyle on 8/31/19.
//

#include "MainScreen.h"
#include "../SdlException.h"
#include "../elements/ButtonElement.h"
#include <iostream>
void MainScreen::draw(SDL_Renderer *renderer) {
    Size windowSize{};
    int err = SDL_GetRendererOutputSize(renderer, &windowSize.w, &windowSize.h);
    if (err != 0) {
        throw SdlException();
    }
    err = setRenderDrawColor(renderer, {10, 20, 30, 255});
    if (err != 0){
        throw SdlException();
    }
    err = SDL_RenderFillRect(renderer, nullptr);
    if (err != 0) {
        throw SdlException();
    }
    myButton_.draw(renderer);
}
MainScreen::MainScreen(SizedFont* font) {
    myButton_ = std::move(ButtonElement(font));
    myButton_.setX(20);
    myButton_.setY(20);
    myButton_.setW(100);
    myButton_.setH(100);
    myButton_.setText("Hello!");
    myButton_.setAlignment({LEFT, TOP});
    myButton_.setClickAction([&](){
        std::cout << "Click!\n";
        myButton_.setVisible(false);
    });
    myButton_.setVisible(true);
    elements_ = std::vector<Element *>();
    elements_.push_back(&myButton_);
    const Uint8* kbState = SDL_GetKeyboardState(nullptr);
    eventHandler = [&](SDL_Event &e) {
        switch (e.type) {
        case SDL_MOUSEWHEEL:
            if (kbState[SDL_SCANCODE_LSHIFT] || kbState[SDL_SCANCODE_RSHIFT]) {
		myButton_.setW(myButton_.getW() + (e.wheel.y * 3));
		myButton_.setH(myButton_.getH() + (e.wheel.x * 3));
	    } else {
                myButton_.setW(myButton_.getW() + (e.wheel.x * 3));
                myButton_.setH(myButton_.getH() + (e.wheel.y * 3));
            }
            break;
        case SDL_KEYDOWN:
        case SDL_KEYUP:
            kbState = SDL_GetKeyboardState(nullptr);
        }
    };
}
