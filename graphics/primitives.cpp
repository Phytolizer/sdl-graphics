//
// Created by kyle on 8/31/19.
//
#include "primitives.h"

int setRenderDrawColor(SDL_Renderer *renderer, const Color &color) {
    return SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}
