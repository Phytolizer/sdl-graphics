//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_TOOLS_H
#define SUDOKU_TOOLS_H

#include "primitives.h"
namespace tools {
bool inBounds(const Rect &bounds, const Point &point);
}

#endif // SUDOKU_TOOLS_H
