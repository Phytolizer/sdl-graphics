//
// Created by kyle on 8/31/19.
//
#include "tools.h"

bool tools::inBounds(const Rect &bounds, const Point &point) {
    return (point.x >= bounds.x && point.y >= bounds.y) &&
           (point.x < (bounds.x + bounds.w) && point.y < (bounds.y + bounds.h));
}
