//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_PRIMITIVES_H
#define SUDOKU_PRIMITIVES_H

#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_stdinc.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
typedef unsigned int uint;

struct Rect {
    int x;
    int y;
    int w;
    int h;
    SDL_Rect sdl() {
        return {x, y, w, h};
    }
};

struct Point {
    int x;
    int y;
    SDL_Point sdl() {
        return {x, y};
    }
};

struct Size {
    int w;
    int h;
    SDL_Point sdl() {
        return {w, h};
    }
};

struct Color {
    Uint8 r;
    Uint8 g;
    Uint8 b;
    Uint8 a;
    SDL_Color sdl() {
        return {r, g, b, a};
    }
};
namespace color {
const Color WHITE = {255, 255, 255, 255};
const Color BLACK = {0, 0, 0, 255};
} // namespace color
int setRenderDrawColor(SDL_Renderer *renderer, const Color &color);

#endif // SUDOKU_PRIMITIVES_H
