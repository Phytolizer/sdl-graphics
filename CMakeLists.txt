cmake_minimum_required(VERSION 3.14)
project(sudoku)

set(CMAKE_CXX_STANDARD 14)

add_executable(sudoku main.cpp SdlException.cpp SdlException.h screens/Screen.h screens/MainScreen.cpp screens/MainScreen.h elements/Element.cpp elements/Element.h elements/Alignment.h graphics/tools.h graphics/primitives.h graphics/tools.cpp elements/ButtonElement.cpp elements/ButtonElement.h graphics/SizedFont.cpp graphics/SizedFont.h TtfException.cpp TtfException.h graphics/primitives.cpp screens/Screen.cpp)

Include(FindPkgConfig)
PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
PKG_SEARCH_MODULE(SDL2TTF REQUIRED SDL2_ttf>=2.0.0)
if (NOT SDL2_FOUND OR NOT SDL2TTF_FOUND)
    message(FATAL_ERROR "Could not find SDL2 and SDL2_ttf. Make sure they are installed")
endif ()
message(${SDL2TTF_LIBRARIES})
#find_package(SDL2 REQUIRED)
#find_package(SDL2TTF REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS} ${SDL2TTF_INCLUDE_DIRS})
#set(LIBS ${LIBS} ${SDL2_LIBRARIES})
TARGET_LINK_LIBRARIES(sudoku ${SDL2_LIBRARIES} ${SDL2TTF_LIBRARIES})

